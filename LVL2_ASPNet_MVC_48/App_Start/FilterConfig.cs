﻿using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_48
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
